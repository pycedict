# -*- coding: utf-8 -*-
#
#  main.py
#  PyCEDict
#
#  Created by foosh on 7/30/08.
#  Copyright __MyCompanyName__ 2008. All rights reserved.
#

#import modules required by application
import objc
import Foundation
import AppKit

from PyObjCTools import AppHelper

# import modules containing classes required to start application and load MainMenu.nib
import PyCEDictAppDelegate
import PyDict

# pass control to AppKit
AppHelper.runEventLoop()
