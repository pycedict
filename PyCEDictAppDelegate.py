# -*- coding: utf-8 -*-
#
#  PyCEDictAppDelegate.py
#  PyCEDict
#
#  Created by foosh on 7/30/08.
#  Copyright __MyCompanyName__ 2008. All rights reserved.
#

from Foundation import *
from AppKit import *

class PyCEDictAppDelegate(NSObject):
    mainWindow = objc.IBOutlet()
    keepWindowOnTopButton = objc.IBOutlet()
    rememberWindowSizeButton = objc.IBOutlet()
    rememberWindowLocationButton = objc.IBOutlet()
    
    
    def applicationDidFinishLaunching_(self, sender):
        NSLog("Application did finish launching.")
        self.mainWindow.setLevel_(NSFloatingWindowLevel)
        self.checkAndApplyUserPreferences_(sender)
        
    def applicationWillTerminate_(self, sender):
        NSLog("Application will terminate.")
        
    def makeWindowOnTop_(self, sender):
        self.mainWindow.setLevel_(NSFloatingWindowLevel)
        NSLog("setting window to float at NSFloatingWindowLevel")
        
    def checkAndApplyUserPreferences_(self, sender):
        NSLog("checkAndApplyUserPreferences_ called")