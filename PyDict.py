#!/usr/bin/env python2.5
# -*- coding: utf-8 -*-

#  PyDict.py
#  PyCEDict
#
#  Created by David on 7/30/08.
#  Copyright (c) 2009 David Fooshee. All rights reserved.
#
from Foundation import *
from AppKit import *
import objc
import codecs
import os

class PyDict (NSObject):
    # Making class instance vars available across the PyObjC bridge:
    textInput = objc.ivar(u"textInput")
    foundMatches = objc.ivar(u"foundMatches")
    simpDict = objc.ivar(u"simpDict")
    tradDict = objc.ivar(u"tradDict")
    boardChangeCount = objc.ivar(u"boardChangeCount")
    board = objc.ivar(u"board")
    userDefaultsDict = objc.ivar(u"userDefaultsDict")
    winCon = objc.ivar(u"winCon")
    t = objc.ivar(u"t")
    
    def init(self):
        self = super(PyDict, self).init()
        if self is None: return None
        NSTimer.scheduledTimerWithTimeInterval_target_selector_userInfo_repeats_(1.0, self, 'setTextOutput:', None, True)
        self.foundMatches = ""
        
        # Process the CEDICT file data to build our dictionaries
        # We'll do this in a separate thread so as not to delay UI boot speed
        t = NSThread.alloc().initWithTarget_selector_object_(self, self.buildDictionaries, None)
        t.start()
        
        NSLog("Initialization completed")
        return self
    
    # Load and read the cedict file linewise, in a unicode format, into a list of definitions:
    try:
        dictFile = codecs.open(os.path.join(NSBundle.mainBundle().resourcePath(), "cedict.u8" ), encoding='utf-8')
    except (IOError, OSError):
        NSLog("Could not open the CEDICT dictionary file.")
        sys.exit(1)
    else:
        dict = dictFile.readlines()
    
    # Making some dictionaries
    tradDict = {}
    simpDict = {}
   
    
            
    # Make a pasteboard object with which to monitor the system pasteboard for changes
    board = NSPasteboard.generalPasteboard()
    
    #Set up defaults
    #ATTN
    userDefaultsDict = {
                   "windowOnTop" : True,
            "rememberWindowSize" : False,
        "rememberWindowLocation" : True }
        
    userDefaults = NSUserDefaults.standardUserDefaults()
    userDefaults.registerDefaults_(userDefaultsDict)
    NSUserDefaultsController.sharedUserDefaultsController().setInitialValues_(userDefaultsDict)
 
    #TESTCODE       
    if userDefaults.objectForKey_("windowOnTop") is True:
        NSLog("window on top is true!!")
    
    NSLog(str(userDefaultsDict))
    #TESTCODE
    
    
    # init the counter to None so we pick up whatever is already on the clipboard at app launch
    boardChangeCount = None  

   
        
        
    #ATTN
    winCon = NSWindowController.alloc().initWithWindowNibName_("MainMenu")
    
    
    #ATTNATTNATTN
    @objc.IBAction
    def keepWindowOnTopToggle_(self, sender):
        NSLog("keepWindowOnTop checkbox was toggled")
        NSLog("value is now:")
        NSLog(str(self.userDefaults.objectForKey_("windowOnTop")))
        
    
    def buildDictionaries(self):
        
        # New thread, needs it's own autoreleasepool
        pool = NSAutoreleasePool.alloc().init()
        
        # Here we build up the Python dictionaries to be used for fast searching.  
        # Each line (definition) in the cedict has the following format:
        # [traditional form] [simplified form] /definitions/
        # ...hence the use of indices [0] and [1] after splitting lines by whitespace
        for line in self.dict:
            tokens = line.split()
            self.tradDict[tokens[0]] = line
            self.simpDict[tokens[1]] = line
        NSLog("Finished building dictionaries")
        
        # Release the pool
        pool.drain()
        
        
    def setTextOutput_(self):
        if self.board.changeCount() != self.boardChangeCount:   # if new data is on the clipboard
            self.boardChangeCount = self.board.changeCount()    # update board changecount
                
            textInBoard = self.board.stringForType_(NSStringPboardType)
            
            if textInBoard != None:
                charText = textInBoard.rstrip()
                charText = charText.split()
                
                self.foundMatches = ""

                # for each whitespace-separated 'token' on the clipboard...
                for token in charText:                          
                    if self.foundMatches != "":
                        self.foundMatches += '========' + '\n\n'
                    else:
                        self.foundMatches = '\n'
                        
                    tokenLength = len(token)
                    matchWasFound = False
                    for c in range(tokenLength):          # move starting iteration point through token
                        for i in range(1, tokenLength+1): # iterate through the length of the token from start pt.
                            if self.tradDict.has_key(token[c:i]):
                                matchWasFound = True
                                self.foundMatches += self.tradDict[token[c:i]] + '\n'
                            elif self.simpDict.has_key(token[c:i]):
                                matchWasFound = True
                                self.foundMatches += self.simpDict[token[c:i]] + '\n'
                    
                    #if not matchWasFound: self.foundMatches += '"%s" not found in dictionary.\n\n' % token
                    if not matchWasFound: self.foundMatches = "Clipboard does not seem to contain any Chinese characters"
                    
                    
                    
                    
                    
